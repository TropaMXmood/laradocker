<?php

namespace App\Http\Controllers;

use App\Repositories\FruitRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class FruitController extends Controller
{
    private $fruitRepository;

    public function __construct(FruitRepository $fruitRepository)
    {
        $this->fruitRepository = $fruitRepository;
    }
    public function getFruits(Request $request){
        $result = $this->fruitRepository->getFruits($request->all());
        if ($result) {
            return \Response::json($result);
        } else return \Response::json(['fail' => 'No Data found']);
    }

    public function storeFruit(Request $request){
        try {
            DB::beginTransaction();
            $flag = $this->fruitRepository->saveFruits($request->all());
            DB::commit();
            if($flag) return \Response::json(['success' => '200']);
        } catch (\Exception $e) {
            DB::rollBack();
            return \Response::json(['error' => 'Error in processing']);
        }
    }
}
