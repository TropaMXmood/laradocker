<?php
namespace App\Repositories;

use App\Fruit;
use App\Repositories\Criteria\Fruit\FruitsByName;
use App\Repositories\Criteria\Fruit\FruitsExcludeByName;
use Illuminate\Support\Facades\Redis;

class FruitRepository extends Repository
{
    /**
     * Specify Model class name
     *
     * @return mixed
     */

    function model()
    {
        return 'App\Fruit';
    }

    public function saveFruits($fruits)
    {
        $flag = 0;
        foreach ($fruits as $key => $value){
            $fruit = Fruit::firstOrNew(array('name' => $key));
            $fruit->color = $value;
            $fruit->save();
            $flag = 1;

            Redis::set($key, $value);
            Redis::expire($key, 60*5);
        }
        return $flag;
    }
    public function getFruits($data){
        $pair = [];
        $keysArr = array();
        if(isset($data['keys'])){
            $keys =  explode(",", $data['keys']);
            if (!empty($keys)) {
                foreach ($keys as $key) {
                    if(!$key) continue;
                    if (Redis::get($key) != "")
                        $pair[$key] = Redis::get($key);
                    else
                        array_push($keysArr,$key);
                }
            }

            $query = $this->pushCriteria(new FruitsByName($keysArr));

        }else {
            $keys = Redis::keys("*");
            if (!empty($keys)) {
                foreach ($keys as $key) {
                    $pair[$key] = Redis::get($key);
                    array_push($keysArr,$key);
                }
            }
            $query = $this->pushCriteria(new FruitsExcludeByName($keysArr));

        }

        $fruitData = $query->applyCriteria()->paginate(20);
        $fruitArr = $fruitData->pluck('color', 'name')->toArray();
        $result = array_merge($pair,$fruitArr);
        return $result;
    }

}