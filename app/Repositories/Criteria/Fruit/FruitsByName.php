<?php


namespace App\Repositories\Criteria\Fruit;

use App\Repositories\Criteria\Criteria;
use App\Repositories\RepositoryInterface as Repository;

class FruitsByName extends Criteria
{
    private $keys;

    public function __construct($keys)
    {
        $this->keys = $keys;
    }

    public function apply($model, Repository $repository)
    {
        $query = $model->whereIn('name', $this->keys);
        return $query;
    }
}